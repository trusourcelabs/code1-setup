//
//  Philips_User_SetupTests.swift
//  Philips User SetupTests
//
//  Created by Bitson on 5/12/16.
//  Copyright © 2016 Trusource Labs. All rights reserved.
//

import XCTest

class Philips_User_SetupTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }

    func test_a_CreatePlaceHolder() {
        let expectation = expectationWithDescription("URL Load")
        let jss = JSSAPI(serverURL: "philips01.jamfcloud.com", jamfUsername: "jssadmin", jamfPassword: "Mum5ah7aewoonu5ioPojux@a")
        jss.createPlaceholder("C02P28QSF8J2", macAddress: "ac:87:a3:26:e2:4d", completionHandler: {(data, response, error) in
            if (response != nil) {
                print(response!)
                let httpResponse = response as! NSHTTPURLResponse
                
                if (httpResponse.statusCode >= 200 && httpResponse.statusCode < 500) {
                    expectation.fulfill()
                }            }
            
            if (data != nil ) {
                print(NSString(data: data!, encoding: NSUTF8StringEncoding)!)
            }
            
            if (error != nil) {
                print(error!)
            }
        })
        
        waitForExpectationsWithTimeout(8.0, handler:nil)
    }
    
    func test_b_AddComputerToGroup() {
        let expectation = expectationWithDescription("URL Load")

        let jss = JSSAPI(serverURL: "philips01.jamfcloud.com", jamfUsername: "jssadmin", jamfPassword: "Mum5ah7aewoonu5ioPojux@a")
        jss.addComputerToGroup("C02P28QSF8J2", groupName: "API Group", completionHandler: { (data, response, error) in
            if (response != nil) {
                print(response!)
                let httpResponse = response as! NSHTTPURLResponse
                
                if (httpResponse.statusCode >= 200 && httpResponse.statusCode < 500) {
                    expectation.fulfill()
                }
            }
            
            if (data != nil ) {
                print(NSString(data: data!, encoding: NSUTF8StringEncoding)!)
            }
            
            if (error != nil) {
                print(error!)
            }
        })
        
        waitForExpectationsWithTimeout(8.0, handler:nil)
    }
    
    // This is commented out since it's deleting the computer from the group -- makes it a tad hard to verify we're creating
    // the record and adding it to a group in the JSS.
    /*
    func test_c_RemoveComputerFromGroup() {
        let expectation = expectationWithDescription("URL Load")
        
        let jss = JSSAPI(serverURL: "philips01.jamfcloud.com", jamfUsername: "jssadmin", jamfPassword: "Mum5ah7aewoonu5ioPojux@a")
        jss.removeComputerFromGroup("C02P28QSF8J2", groupName: "API Group", completionHandler: { (data, response, error) in
            if (response != nil) {
                print(response!)
                let httpResponse = response as! NSHTTPURLResponse
                
                if (httpResponse.statusCode >= 200 && httpResponse.statusCode < 500) {
                    expectation.fulfill()
                }
            }
            
            if (data != nil ) {
                print(NSString(data: data!, encoding: NSUTF8StringEncoding)!)
            }
            
            if (error != nil) {
                print(error!)
            }
        })
        
        waitForExpectationsWithTimeout(8.0, handler:nil)
    }
    */
}
