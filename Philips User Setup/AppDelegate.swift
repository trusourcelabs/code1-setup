//
//  AppDelegate.swift
//  Philips User Setup
//
//  Created by Admin on 3/24/16.
//  Copyright © 2016 Trusource Labs. All rights reserved.
//

import Cocoa

let log = SwiftyBeaver.self

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {
   
let jss = JSSAPI(serverURL: "philips02.jamfcloud.com", jamfUsername: "setup_tool_user", jamfPassword: "Aec-NG3-yLc-yTc!")
    
    let mySerial = getSerial()
    //let mySerial = "C02KC1XFDRVG"
    
    @IBAction func GetMachineInfo(sender: AnyObject) {
        NSLog("Get machine info")
        
        var computerStatus = ""
        var enrolledUser = ""
        
        jss.getComputerRecord(mySerial,completionHandler: {(data, response, error) in
            if (response != nil) {
                print(response!)
                let httpResponse = response as! NSHTTPURLResponse
                
                if (httpResponse.statusCode >= 200 && httpResponse.statusCode < 500) {
                }            }
            
            if (data != nil ) {
                log.verbose(NSString(data: data!, encoding: NSUTF8StringEncoding)!)
                
                let computerRecordFull = String(data: data!, encoding: NSUTF8StringEncoding)!
                
                if computerRecordFull.containsString("<computers><size>0</size>") {
                    computerStatus = "Not Enrolled"
                }
                else {
                    
                    let frontRegEx: NSRegularExpression?
                    let backRegEx: NSRegularExpression?
                    
                    do {
                        frontRegEx = try NSRegularExpression(pattern: ".*<realname>", options: NSRegularExpressionOptions.CaseInsensitive)} catch {
                            frontRegEx = nil
                    }
                    do {
                        backRegEx = try NSRegularExpression(pattern: "</realname>.*", options: NSRegularExpressionOptions.CaseInsensitive)} catch {
                            backRegEx = nil
                    }
                    
                    enrolledUser = frontRegEx!.stringByReplacingMatchesInString(computerRecordFull, options: [], range: NSMakeRange(0, computerRecordFull.characters.count), withTemplate: "")
                    enrolledUser = backRegEx!.stringByReplacingMatchesInString(enrolledUser, options: [], range: NSMakeRange(0, enrolledUser.characters.count), withTemplate: "")
                    
                    print(enrolledUser)
                    
                    // log.verbose(self.JSSGroups)
                    computerStatus = "Enrolled"
                }
            }
            
            if (error != nil) {
                log.verbose(error!)
                computerStatus = "An unfortunate error occured."
            }
            let infoAlert = NSAlert()
            infoAlert.messageText = computerStatus + "\n" + enrolledUser
            infoAlert.runModal()
            
        })

    }

    @IBAction func EnrollOnly(sender: AnyObject) {
        
        let myBundle = NSBundle.mainBundle()
        let scriptPath = myBundle.pathForResource("QuickAdd", ofType: "pkg")
        let scriptCommand = "do shell script \"/usr/sbin/installer -allowUntrusted -pkg /private/tmp/QuickAdd.pkg -target /\" with administrator privileges"
        print (scriptCommand)
        
        // get other variables
        // execute shell script via very ugly sudo method
        
        let fileManager = NSFileManager()
        do {
            try fileManager.copyItemAtPath(scriptPath!, toPath: "/private/tmp/QuickAdd.pkg")} catch {}
        
        NSAppleScript(source: scriptCommand )!.executeAndReturnError(nil)
    }


    func applicationDidFinishLaunching(aNotification: NSNotification) {
        
        // add log destinations. at least one is needed!
        let console = ConsoleDestination()  // log to Xcode Console
        let file = FileDestination()  // log to default swiftybeaver.log file
        log.addDestination(console)
        log.addDestination(file)
        
        
        let platform = SBPlatformDestination(appID: "LQnnJd", appSecret: "nv7uekmynpnaaKIeDsttmxxvilfrFYpP", encryptionKey: "2uasTEtqdk5zfbrjto7vx0lm26jcUuit")
        log.addDestination(platform)
        
        // debug settings to get the logs to the cloud quicker
        
        platform.asynchronously = true
        //platform.showNSLog = true
        
        platform.analyticsUserName = getConsoleUser()
        
        log.verbose("Finished launching")
    }

    func applicationWillTerminate(aNotification: NSNotification) {
        // Insert code here to tear down your application
    }


}

