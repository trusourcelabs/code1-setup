#!/bin/bash

# get the supplied arguments

while getopts j: SWITCH
do
    case $SWITCH in
        j) JAMFServer=$OPTARG;;
    esac
done

####################################################
## Explode the binary and move it into the right place
####################################################
/usr/bin/tar -xf ./jamf.zip -C /private/tmp/
/bin/mkdir -p /usr/local/jamf/bin
/bin/mv /private/tmp/jamf /usr/local/jamf/bin/jamf

echo "Creating symlink $BINARY_SYMLINK_PATH"

if [ ! -e $BINARY_SYMLINK_DIR ]; then
echo "Creating $BINARY_SYMLINK_DIR"
/bin/mkdir -p $BINARY_SYMLINK_DIR
fi

/usr/sbin/chown 0:0 $jamfCLIPath
/bin/chmod 551 $jamfCLIPath

echo 'Setting permissions'
/bin/chmod 551 $BINARY_PATH
echo 'Starting ssh...'
$BINARY_EXEC startSSH

echo 'Creating config file...'
echo 'Allowing for invalid certificate...'


####################################################
## Create the configuration file at:
## /Library/Preferences/com.jamfsoftware.jamf.plist
####################################################

case "$JAMFServer" in
    philips01) $jamfCLIPath createConf -url 'https://philips01.jamfcloud.com' -k;;
    philips02) $jamfCLIPath createConf -url 'https://philips02.jamfcloud.com' -k;;
    *) echo "No server by that name."
esac

####################################################
## Run enroll
####################################################

case $JAMFServer in
    philips01) $jamfCLIPath enroll -invitation 57164312187982661358956520959406356015;;
    philips02) $jamfCLIPath enroll -invitation 321688581655825137842043614024310001323;;
    *) echo "No server by that name."
esac

enrolled=$?

if [ $enrolled -eq 0 ]
then
echo 'Enrollment Successful. Proceeding...'
else
echo 'Enrollment Failed. This PKG may be used already.'
fi

/bin/rm -rf /private/tmp/jamf
exit $enrolled




#!/bin/bash

BINARY_TMP_PATH=/private/tmp/jamf
BINARY_PATH=/usr/local/jamf/bin/jamf
BINARY_DIR=$(dirname $BINARY_PATH)
BINARY_SYMLINK_PATH=/usr/local/bin/jamf
BINARY_SYMLINK_DIR=$(dirname $BINARY_SYMLINK_PATH)
BINARY_EXEC=$BINARY_SYMLINK_PATH
JSS_URL='https://philips01.jamfcloud.com'

echo "Creating symlink $BINARY_SYMLINK_PATH"

if [ ! -e $BINARY_SYMLINK_DIR ]; then
echo "Creating $BINARY_SYMLINK_DIR"
/bin/mkdir -p $BINARY_SYMLINK_DIR
fi

/bin/ln -sf $BINARY_PATH $BINARY_SYMLINK_PATH

echo 'Setting permissions'
/bin/chmod 551 $BINARY_PATH
echo 'Starting ssh...'
$BINARY_EXEC startSSH

echo 'Creating config file...'
echo 'Allowing for invalid certificate...'
$BINARY_EXEC createConf -k -url $JSS_URL

echo 'Enrolling device...'
$BINARY_EXEC enroll -invitation 78103895187295859323376625571109968407
enrolled=$?
if [ $enrolled -eq 0 ]
then
echo 'Enrollment Successful. Proceeding...'
else
echo 'Enrollment Failed. This PKG may be used already.'
fi

exit $enrolled
