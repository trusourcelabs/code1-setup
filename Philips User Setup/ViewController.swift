//
//  ViewController.swift
//  Philips User Setup
//
//  Created by Admin on 3/24/16.
//  Copyright © 2016 Trusource Labs. All rights reserved.
//

import Cocoa
import SystemConfiguration

class ViewController: NSViewController {

    @IBOutlet weak var userNameField: NSTextField?
    @IBOutlet weak var Code1IDField: NSTextField?
    @IBOutlet weak var SetupButton: NSButton?
    @IBOutlet weak var emailAddress: NSTextField?
    @IBOutlet weak var DEPGroupList: NSPopUpButton!
    @IBOutlet weak var ProgressIndicator: NSProgressIndicator!
    @IBOutlet weak var ProgressText: NSTextField!
    
    // Globals
    
    private var myConsoleUser: String = ""
    private var myCode1ID: String = ""
    private var myMachineData: String = ""
    
    var JSSGroups = [String]()
    //var JSSServer = "philips01.jamfcloud.com"
    var JSSServer = "philips02.jamfcloud.com"
    
    // set up the JSS connection - keep it secret, keep it safe
    
    // philips01 user
    
   // let jss = JSSAPI(serverURL: "philips01.jamfcloud.com", jamfUsername: "jssadmin", jamfPassword: "Mum5ah7aewoonu5ioPojux@a")
    
    // philips02 user
    
    let jss = JSSAPI(serverURL: "philips02.jamfcloud.com", jamfUsername: "setup_tool_user", jamfPassword: "Aec-NG3-yLc-yTc!")
    
    // set up the completion handler
    
    let myCompletionHandler: (NSData?, NSURLResponse?, NSError?) -> Void = {(data, response, error) in
        if (response != nil) {
            print(response!)
            let httpResponse = response as! NSHTTPURLResponse
            
            if (httpResponse.statusCode >= 200 && httpResponse.statusCode < 500) {
            }            }
        
        if (data != nil ) {
            log.verbose(NSString(data: data!, encoding: NSUTF8StringEncoding)!)
        }
        
        if (error != nil) {
            log.error(error!)
        }
    }

    // Functions
    
    // This checks to see if the user has a prepended a and helpfully removes it
    
    func getCode1FromConsole(consoleUser: String) -> String {
      if consoleUser.hasPrefix("a") && consoleUser.characters.count == 10 {
            let myCode1 = consoleUser[consoleUser.startIndex.advancedBy(1)..<consoleUser.endIndex]
            return myCode1
        }
        else {
            return consoleUser
        }
    
    }
    
    // This writes data to a file
    
    func writeData(filePath: String, fileData: String){
        
            //writing
            do {
                try fileData.writeToFile(filePath, atomically: false, encoding: NSUTF8StringEncoding)
            }
            catch {/* error handling here */}

    }
    
    // This gathers up some system info for pushing to the ARD variables
    
    func gatherSystemInfo() -> String {
        
        // get a Process Info object
        let myProcess = NSProcessInfo.processInfo()
        
        // now query that object to get some information
        
        let myHostname = myProcess.hostName
        let myMemory = String(myProcess.physicalMemory / 1073741824)
        let myProcessors = String(myProcess.processorCount)
        let myOS = myProcess.operatingSystemVersionString
        
        // now for some dirty dirty NSTask -> sysctl work since using sysctl directly is more work than it should be
        
        let hwInfo = cliTask("/usr/sbin/sysctl machdep.cpu.brand_string")
        let model = hwInfo[hwInfo.startIndex.advancedBy(26)..<hwInfo.endIndex]
        let formatedModel = model.stringByTrimmingCharactersInSet(NSCharacterSet.newlineCharacterSet())
        
        print(myHostname, "\n",myMemory, "GB RAM\n",formatedModel, "\n",myProcessors, " cores\n",myOS )
        let myMachineData = myHostname + "," + myMemory + "GB RAM," + myProcessors + " cores," + myOS
        
        let filePath = "/tmp/machineInfo.txt"
        writeData(filePath, fileData: myMachineData)
        
        // return the info
        log.verbose(myMachineData)
        return myMachineData
        
    }
    
    // On opening the application
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // get system info
        myConsoleUser = getConsoleUser()
        myMachineData = gatherSystemInfo()
        let userName = getConsoleUser()
        userNameField!.stringValue = NSFullUserName()
        myCode1ID = getCode1FromConsole(userName)
        Code1IDField!.stringValue = myCode1ID as String
        
        ProgressText.stringValue = "Getting groups..."
        ProgressIndicator.indeterminate = true
        
        ProgressIndicator.startAnimation(self)
        
        // get DEP groups from the JSS
        
        jss.getGroups({(data, response, error) in
            if (response != nil) {
                print(response!)
                let httpResponse = response as! NSHTTPURLResponse
                
                if (httpResponse.statusCode >= 200 && httpResponse.statusCode < 500) {
                }            }
            
            if (data != nil ) {
                //log.verbose(NSString(data: data!, encoding: NSUTF8StringEncoding)!)
                
                let groupListXML = String(data: data!, encoding: NSUTF8StringEncoding)!.componentsSeparatedByString("</computer_group>")
                
                let frontRegEx: NSRegularExpression?
                let backRegEx: NSRegularExpression?
                
                do {
                    frontRegEx = try NSRegularExpression(pattern: ".*<name>", options: NSRegularExpressionOptions.CaseInsensitive)} catch {
                        frontRegEx = nil
                }
                do {
                    backRegEx = try NSRegularExpression(pattern: "</name>.*", options: NSRegularExpressionOptions.CaseInsensitive)} catch {
                        backRegEx = nil
                }
                
                for line in groupListXML {
                    if line.containsString("DEP-") {
                        var workingLine: String = frontRegEx!.stringByReplacingMatchesInString(line, options: [], range: NSMakeRange(0, line.characters.count), withTemplate: "")
                        workingLine = backRegEx!.stringByReplacingMatchesInString(workingLine, options: [], range: NSMakeRange(0, workingLine.characters.count), withTemplate: "")
                        print(workingLine)
                        self.JSSGroups.insert(workingLine, atIndex: 0)
                        
                    }
                }
               // log.verbose(self.JSSGroups)
                self.DEPGroupList.addItemsWithTitles(self.JSSGroups)
                self.ProgressIndicator.stopAnimation(nil)
                self.ProgressIndicator.indeterminate = false
                self.ProgressText.stringValue = "Ready."
                self.SetupButton?.enabled = true
            }
            
            if (error != nil) {
                log.verbose(error!)
                let alertController = NSAlert()
                alertController.messageText = "JAMF server can not be contacted. Please try again later."
                alertController.beginSheetModalForWindow(self.view.window!, completionHandler: nil)
                self.ProgressIndicator.indeterminate = false
                self.ProgressText.stringValue = "Can't contact JAMF server."
                self.ProgressIndicator.stopAnimation(nil)
            }
        })
        
        log.verbose("Starting up.")
        log.verbose("user: " + myConsoleUser)
        

    }
    
    override var representedObject: AnyObject? {
        didSet {
        // Update the view, if already loaded.
        }
    }
    
    // When the button is clicked

    @IBAction func SetupClicked(sender: NSButton) {
        
        // Get the information from the UI
        let myEmailAddress = emailAddress!.stringValue
        myCode1ID = Code1IDField!.stringValue
        
        // Write out the ARD options
        // NOTE: this only works if the machine hasn't been used before
        // we don't write this plist as root, so the file has to not exist
        
        let myCompanyText = "Property of Philips"
        
        ProgressText.stringValue = "Setting computer information."
        ProgressIndicator.incrementBy(10)

        log.verbose("Adding ARD attributes: " + NSFullUserName() + ", " + myCode1ID)
      cliTask("/usr/bin/defaults write /Library/Preferences/com.apple.RemoteDesktop Text1 \"" + NSFullUserName().stringByReplacingOccurrencesOfString(" ", withString: "\\ ") + "\"")
        cliTask("/usr/bin/defaults write /Library/Preferences/com.apple.RemoteDesktop Text2 \"" + myCode1ID + "\"")
        cliTask("/usr/bin/defaults write /Library/Preferences/com.apple.RemoteDesktop Text3 \"" + myCompanyText.stringByReplacingOccurrencesOfString(" ", withString: "\\ ") + "\"")
        cliTask("/usr/bin/defaults write /Library/Preferences/com.apple.RemoteDesktop Text4 \"" + myMachineData.stringByReplacingOccurrencesOfString(" ", withString: "\\ ") + "\"")
        
        // get the currently selected group
        
        let DEPGroup = DEPGroupList.selectedItem?.title
        var removeGroups = JSSGroups
        
        removeGroups.removeAtIndex(DEPGroupList.indexOfSelectedItem)
        
        // now to add the computer
        
        ProgressText.stringValue = "Adding placeholder."
        ProgressIndicator.incrementBy(10)
        
        let hardwareSerial = getSerial()
        let hardwareMAC = getMAC()
        
        log.verbose("Serial: " + hardwareSerial)
        log.verbose("MAC Address: " + hardwareMAC)
        
        // create the machine placeholder, ignoring if it's already enrolled
        
        log.verbose("Creating Placeholder: " + hardwareSerial + ", " + hardwareMAC)
        jss.createPlaceholder(hardwareSerial, macAddress: hardwareMAC, completionHandler: myCompletionHandler)
        
        // remove it from any groups they didn't pick
        
        log.verbose("Removing from groups: " + String(removeGroups) )
        ProgressText.stringValue = "Removing other groups."
        ProgressIndicator.incrementBy(20)
        
        for group in removeGroups {
            jss.removeComputerFromGroup(hardwareSerial, groupName: group, completionHandler: myCompletionHandler)
        }
    
        // this is ugly and should be fixed
        sleep(5)
        
        // now add it to the selected group
        
        NSLog("Adding to group: " + DEPGroup!)
        log.verbose("Adding to group: " + DEPGroup!)
        ProgressText.stringValue = "Adding computer to group."
        ProgressIndicator.incrementBy(20)
        
        jss.addComputerToGroup(hardwareSerial, groupName: DEPGroup!, completionHandler: {(data, response, error) in
            if (response != nil) {
                print(response!)
                let httpResponse = response as! NSHTTPURLResponse
                
                if (httpResponse.statusCode >= 200 && httpResponse.statusCode < 500) {
                }            }
            
            if (data != nil ) {
                //log.verbose(NSString(data: data!, encoding: NSUTF8StringEncoding)!)

                let myBundle = NSBundle.mainBundle()
                let scriptPath = myBundle.pathForResource("QuickAdd", ofType: "pkg")
                let scriptCommand = "do shell script \"/usr/sbin/installer -allowUntrusted -pkg /private/tmp/QuickAdd.pkg -target /\" with administrator privileges"
                print (scriptCommand)
                //log.verbose(scriptCommand)
                
                self.ProgressText.stringValue = "Enrolling computer in JAMF."
                self.ProgressIndicator.incrementBy(20)
                
                // get other variables
                // execute shell script via very ugly sudo method
                
                let fileManager = NSFileManager()
                do {
                    try fileManager.copyItemAtPath(scriptPath!, toPath: "/private/tmp/QuickAdd.pkg")} catch {}
                
                NSAppleScript(source: scriptCommand )!.executeAndReturnError(nil)
                
                self.ProgressText.stringValue = "Enrolled!"
                self.ProgressIndicator.incrementBy(20)
                log.verbose("Enrolled")

            }
            
            if (error != nil) {
                log.verbose(error!)
            }
        })

    }

}

